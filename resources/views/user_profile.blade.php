@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Profile Card</div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-5">
                            <h1>{{$item->first_name}} {{$item->last_name}} <small>(<a href="mailto:{{$item->email}}">{{$item->email}}</a>)</small></h1>
                            @if($item->phone != '')
                                <h3><a href="tel:{{$item->phone}}">{{$item->phone}}</a></h3>
                            @endif
                            @if($item->address != '' && $item->city_name != '' && $item->state_name != '')
                                <address>{{$item->address}}<br>{{$item->city_name}}, {{$item->state_name}} {{$item->zip}}<br>{{$item->country_name}}</address>
                            @endif
                            @if($item->birthday != '0000-00-00')
                                <div class="well">
                                    <p><i class="fa fa-calendar fa-lg"></i> <u>Birthday:</u> {{ date('F d, Y', strtotime($item->birthday))}}</p>
                                </div>
                            @endif
                            <table class="table">
                                <tr><td><strong>Created:</strong></td><td>{{ date('F d, Y', strtotime($item->created_at))}}</td></tr>
                                <tr><td><strong>Last Updated:</strong></td><td>{{ date('F d, Y', strtotime($item->updated_at))}}</td></tr>
                            </table>
                        </div>
                        <div class="col-md-7">
                            @if($item->lat != '' && $item->lon != '' && $item->address != '')
                                <div id="the_map"></div>
                            @endif
                        </div>
                    </div>
                    @if($item->twitter_id != '')
                        <div class="row">
                            <div class="col-md-12">
                                <a class="twitter-timeline" data-widget-id="705423524714811392" href="https://twitter.com/{{$item->twitter_id}}" data-screen-name="{{$item->twitter_id}}">Tweets by &#64;{{$item->twitter_id}}</a>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('header_content')
    @parent

    <style>
        #the_map {
            width: 100%;
            height: 475px;
            border: 1px solid #333;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }
    </style>
@endsection

@section('footer_content')
    @parent

    @if($item->twitter_id != '')
        <script>
            window.twttr = (function(d, s, id) {
              var js, fjs = d.getElementsByTagName(s)[0],
                t = window.twttr || {};
              if (d.getElementById(id)) return t;
              js = d.createElement(s);
              js.id = id;
              js.src = "https://platform.twitter.com/widgets.js";
              fjs.parentNode.insertBefore(js, fjs);

              t._e = [];
              t.ready = function(f) {
                t._e.push(f);
              };

              return t;
            }(document, "script", "twitter-wjs"));
        </script>
    @endif

    @if($item->lat != '' && $item->lon != '' && $item->address != '')
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?language=en"></script>
        <script type="text/javascript" src="{{ url('/assets/js/gmap3.min.js') }}"></script>
        <script>
            $("#the_map").gmap3({
                map: {
                    options: {
                        center: [{{$item->lat}}, {{$item->lon}}],
                        zoom: 15
                    }
                },
                marker: {
                    values: [
                        {latLng: [{{$item->lat}}, {{$item->lon}}], data: '{{$item->first_name}} {{$item->last_name}}'}
                    ],
                    options: {
                        draggable: false
                    },
                    events: {
                        mouseover: function(marker, event, context){
                            var map = $(this).gmap3("get"),
                                infowindow = $(this).gmap3({get: {name:"infowindow"}});
                            if(infowindow){
                                infowindow.open(map, marker);
                                infowindow.setContent(context.data);
                            }else{
                                $(this).gmap3({
                                    infowindow: {
                                        anchor: marker,
                                        options: {context: context.data}
                                    }
                                });
                            }
                        },
                        mouseout: function(){
                            var infowindow = $(this).gmap3({get: {name:"infowindow"}});
                            if(infowindow){
                                infowindow.close();
                            }
                        }
                    }
                }
            });
        </script>
    @endif

@endsection