@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome</div>

                <div class="panel-body">
                    <p>Welcome to Contacts!</p>
                    @if(Auth::guest())
                        <p>If you have not yet, please <a href="{{ url('/register') }}">Create an Account</a> to get started. Otherwise, you may <a href="{{ url('/login') }}">login here</a>.</p>
                    @else
                        <p>You may manage your Contacts <a href="{{ url('/contacts') }}">here</a>.</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
