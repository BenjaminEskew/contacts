@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Account Details (<a href="{{ url('/profile') }}/{{ Auth::user()->id }}">View Your Profile</a>)</div>

                <div class="panel-body">
                    <form class="form-horizontal" name="edit-profile" method="post" action="{{ url('/edit-account') }}" autocomplete="off">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input type="text" name="first_name" value="{{ Auth::user()->first_name }}" class="form-control" placeholder="John" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <input type="text" name="last_name" value="{{ Auth::user()->last_name }}" class="form-control" placeholder="Doe" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nick Name</label>
                                    <input type="text" name="name" value="{{ Auth::user()->name }}" class="form-control" placeholder="Nick">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" name="email" value="{{ Auth::user()->email }}" class="form-control" placeholder="john.doe@gmail.com" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Phone</label>
                                    <input type="tel" name="phone" value="{{ Auth::user()->phone }}" class="form-control" placeholder="(909) 555-5555">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Birthday</label>
                                    <input type="date" name="birthday" value="{{ Auth::user()->birthday }}" class="form-control" placeholder="1984-05-13" min="1900-01-01" max="2003-01-01">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Street Address</label>
                                    <input type="text" name="address" value="{{ Auth::user()->address }}" class="form-control" placeholder="8484 Sunset Blvd.">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>City Name</label>
                                    <input type="text" name="city_name" value="{{ Auth::user()->city_name }}" class="form-control" placeholder="Hollywood">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>State Name</label>
                                    <input type="text" name="state_name" value="{{ Auth::user()->state_name }}" class="form-control" placeholder="California">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Country Name</label>
                                    <input type="text" name="country_name" value="{{ Auth::user()->country_name }}" class="form-control" placeholder="USA">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Zip Code</label>
                                    <input type="text" name="zip" value="{{ Auth::user()->zip }}" class="form-control" placeholder="90028">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Twitter Username</label>
                                    <input type="text" name="twitter_id" value="{{ Auth::user()->twitter_id }}" class="form-control" placeholder="HavenAgency">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6"></div>
                            <div class="col-md-6">
                                <input type="submit" value="Update" class="btn btn-primary">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="well">
                                    {{ Auth::user()->lat }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="well">
                                    {{ Auth::user()->lon }}
                                </div>
                            </div>
                        </div>
                        {!! csrf_field() !!}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('header_content')

    <style>
        .form-horizontal .row {
            padding: 5px;
        }

    </style>

@endsection