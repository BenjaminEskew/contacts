@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Contact ({{$item->email}})</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="post" action="{{ url('/contact-edit') }}/{{$item->id}}" role="form" style="padding:5px;" autocomplete="off">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input type="text" name="first_name" class="form-control" placeholder="First Name" value="{{$item->first_name}}" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <input type="text" name="last_name" class="form-control" placeholder="Last Name" value="{{$item->last_name}}" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" name="email" class="form-control" placeholder="john.doe@email.com" value="{{$item->email}}" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Phone</label>
                                    <input type="tel" name="phone" class="form-control" placeholder="(909) 555-5555" value="{{$item->phone}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Street Address</label>
                                    <input type="text" name="address" class="form-control" placeholder="6464 Sunset Blvd." value="{{$item->address}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>City Name</label>
                                    <input type="text" name="city_name" class="form-control" placeholder="Hollywood" value="{{$item->city_name}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>State Name</label>
                                    <input type="text" name="state_name" class="form-control" placeholder="California" value="{{$item->state_name}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Zip Code</label>
                                    <input type="text" name="zip" class="form-control" placeholder="90028" value="{{$item->zip}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Country Name</label>
                                    <input type="text" name="country_name" class="form-control" placeholder="USA" value="{{$item->country_name}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Birthday</label>
                                    <input type="date" name="birthday" class="form-control" placeholder="01/01/1983" value="{{$item->birthday}}" min="1900-01-01" max="2003-01-01">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Twitter Username</label>
                                    <input type="text" name="twitter_id" class="form-control" placeholder="HavenAgency" value="{{$item->twitter_id}}">
                                </div>
                            </div>
                            <div class="col-md-6" style="text-align:right;">
                                <input type="submit" class="btn btn-primary" value="Update Contact">
                            </div>
                        </div>
                        {!! csrf_field() !!}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection