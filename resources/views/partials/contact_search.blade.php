<div class="row" id="search-box">
    <div class="col-md-12">
        <form class="form-horizontal" method="get" action="javascript:void(0);" autocomplete="off">
            <div class="row">
                <div class="col-md-11">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon">Search:</span>
                            <input type="search" class="form-control" name="search_text" id="search_text">
                        </div>
                    </div>
                </div>
                <div class="col-md-1">
                    <a href="javascript:void(0);" class="btn btn-default" id="search-go"><i class="fa fa-search"></i></a>
                </div>
            </div>
        </form>
        <div id="search-results-container" class="panel panel-default">
            <div class="panel-body">
                <div id="search-results"></div>
            </div>
        </div>
    </div>
</div>