<div id="add-popup" class="white-popup mfp-hide">
    <form class="form-horizontal" method="post" action="{{ url('/contact-add') }}" role="form" autocomplete="off">
        <h2><i class="fa fa-plus"></i> Add Contact</h2>
        <div class="controls contact-add-form">
            <div class="row">
                <div class="col-xs-6">
                    <input type="text" name="first_name" id="first_name" class="form-control" placeholder="First Name" title="First Name" required>
                </div>
                <div class="col-xs-6">
                    <input type="text" name="last_name" class="form-control" placeholder="Last Name" title="Last Name" required>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <input type="email" name="email" class="form-control" placeholder="john.doe@email.com" title="Email Address" required>
                </div>
                <div class="col-xs-6">
                    <input type="tel" name="phone" class="form-control" placeholder="1-909-555-5555" title="Phone Number">
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <input type="text" name="address" class="form-control" placeholder="6464 Sunset Blvd." title="Street Address">
                </div>
                <div class="col-xs-6">
                    <input type="text" name="city_name" class="form-control" placeholder="Hollywood" title="City Name">
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <input type="text" name="state_name" class="form-control" placeholder="California" title="State Name">
                </div>
                <div class="col-xs-6">
                    <input type="text" name="zip" class="form-control" placeholder="90028" title="Zip Code">
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <input type="text" name="country_name" class="form-control" placeholder="USA" title="Country Name">
                </div>
                <div class="col-xs-6">
                    <input type="date" name="birthday" class="form-control" placeholder="1982-05-13" title="Birthday" min="1900-01-01" max="2003-01-01">
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <input type="text" name="twitter_id" class="form-control" placeholder="HavenAgency" title="Twitter Username">
                </div>
                <div class="col-xs-6" style="text-align:right;">
                    <input type="submit" class="btn btn-primary" value="Add New Contact">
                </div>
            </div>
        </div>
        {!! csrf_field() !!}
    </form>
</div>