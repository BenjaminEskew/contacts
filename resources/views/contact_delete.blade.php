@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Delete Contact ({{$item->email}})</div>

                <div class="panel-body">
                    <form method="post" action="{{ url('/contact-delete') }}/{{$item->id}}" role="form">
                        <div class="well">
                            <h2>Are you sure you want to Delete this Contact?</h2>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>First Name</label>
                                        {{$item->first_name}}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Last Name</label>
                                        {{$item->last_name}}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        {{$item->email}}
                                    </div>
                                </div>
                                <div class="col-md-6">

                                </div>
                            </div>
                            <input type="submit" class="btn btn-danger" value="Yes, Delete Contact">
                        </div>
                        {!! csrf_field() !!}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection