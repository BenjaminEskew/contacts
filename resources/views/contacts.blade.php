@extends('layouts.app')

@section('content')
<div class="container">

    @include('partials.contact_add')

    @include('partials.contact_search')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Contact List [{{$contacts->total()}}] (<a href="#add-popup" class="open-popup-link"><i class="fa fa-plus-square"></i> Add</a> / <a href="javascript:void(0);" onclick="javascript:$('#search-box').toggle();$('#search_text').focus();"><i class="fa fa-search"></i> Search</a>)</div>

                <div class="panel-body">
                    @if(count($contacts) >= 1)
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>
                                        <?php
                                            $sort_lnk = 'id-desc';
                                            $sort_ico = 'fa-sort-numeric-asc';
                                            $sort_txt = 'Descending';
                                            switch($sort)
                                            {
                                                case 'id-desc':
                                                    $sort_lnk = 'id-asc';
                                                    $sort_ico = 'fa-sort-numeric-desc';
                                                    $sort_txt = 'Ascending';
                                                    break;
                                            }
                                        ?>
                                        <a href="{{ url('/contacts') }}?sort={{$sort_lnk}}&amp;limit={{$limit}}" title="Sort by {{$sort_txt}}"><i class="fa {{$sort_ico}}"></i></a>
                                        ID
                                    </th>
                                    <th>
                                        <?php
                                            $sort_lnk = 'fn-desc';
                                            $sort_ico = 'fa-sort-alpha-asc';
                                            $sort_txt = 'Descending';
                                            switch($sort)
                                            {
                                                case 'fn-desc':
                                                    $sort_lnk = 'fn-asc';
                                                    $sort_ico = 'fa-sort-alpha-desc';
                                                    $sort_txt = 'Ascending';
                                                    break;
                                            }
                                        ?>
                                        <a href="{{ url('/contacts') }}?sort={{$sort_lnk}}&amp;limit={{$limit}}" title="Sort by {{$sort_txt}}"><i class="fa {{$sort_ico}}"></i></a>
                                        First Name
                                    </th>
                                    <th>
                                        <?php
                                            $sort_lnk = 'ln-desc';
                                            $sort_ico = 'fa-sort-alpha-asc';
                                            $sort_txt = 'Descending';
                                            switch($sort)
                                            {
                                                case 'ln-desc':
                                                    $sort_lnk = 'ln-asc';
                                                    $sort_ico = 'fa-sort-alpha-desc';
                                                    $sort_txt = 'Ascending';
                                                    break;
                                            }
                                        ?>
                                        <a href="{{ url('/contacts') }}?sort={{$sort_lnk}}&amp;limit={{$limit}}" title="Sort by {{$sort_txt}}"><i class="fa {{$sort_ico}}"></i></a>
                                        Last Name
                                    </th>
                                    <th>
                                        <?php
                                            $sort_lnk = 'em-desc';
                                            $sort_ico = 'fa-sort-alpha-asc';
                                            $sort_txt = 'Descending';
                                            switch($sort)
                                            {
                                                case 'em-desc':
                                                    $sort_lnk = 'em-asc';
                                                    $sort_ico = 'fa-sort-alpha-desc';
                                                    $sort_txt = 'Ascending';
                                                    break;
                                            }
                                        ?>
                                        <a href="{{ url('/contacts') }}?sort={{$sort_lnk}}&amp;limit={{$limit}}" title="Sort by {{$sort_txt}}"><i class="fa {{$sort_ico}}"></i></a>
                                        Email
                                    </th>
                                    <th>
                                        <?php
                                            $sort_lnk = 'ph-desc';
                                            $sort_ico = 'fa-sort-numeric-asc';
                                            $sort_txt = 'Descending';
                                            switch($sort)
                                            {
                                                case 'ph-desc':
                                                    $sort_lnk = 'ph-asc';
                                                    $sort_ico = 'fa-sort-numeric-desc';
                                                    $sort_txt = 'Ascending';
                                                    break;
                                            }
                                        ?>
                                        <a href="{{ url('/contacts') }}?sort={{$sort_lnk}}&amp;limit={{$limit}}" title="Sort by {{$sort_txt}}"><i class="fa {{$sort_ico}}"></i></a>
                                        Phone
                                    </th>
                                    <th>Location</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($contacts as $item)
                                    <tr>
                                        <td>{{$item->id}}</td>
                                        <td>{{$item->first_name}}</td>
                                        <td>{{$item->last_name}}</td>
                                        <td>
                                            @if($item->email != '')
                                                <a href="mailto:{{$item->email}}">{{$item->email}}</a>
                                            @endif
                                        </td>
                                        <td>
                                            @if($item->phone != '')
                                                <a href="tel:{{$item->phone}}">{{$item->phone}}</a>
                                            @endif
                                        </td>
                                        <td>
                                            @if($item->city_name != '')
                                                {{$item->city_name}}@if($item->state_name != ''), @endif
                                            @endif
                                            @if($item->state_name != '')
                                                {{$item->state_name}}
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{ url('/contact') }}/{{$item->id}}" class="btn btn-primary btn-sm"><i class="fa fa-user"></i> View</a>
                                            <a href="{{ url('/contact-edit') }}/{{$item->id}}" class="btn btn-default btn-sm"><i class="fa fa-pencil"></i> Edit</a>
                                            <a href="{{ url('/contact-delete') }}/{{$item->id}}" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <p>Showing {{$contacts->perPage()}} Contacts per page.</p>
                        <p>Show <a href="{{ url('/contacts') }}?sort={{$sort}}&amp;limit=10">10</a> / <a href="{{ url('/contacts') }}?sort={{$sort}}&amp;limit=20">20</a> / <a href="{{ url('/contacts') }}?sort={{$sort}}&amp;limit=50">50</a> per page.</p>
                        {!! $contacts->links() !!}
                    @else
                        <div class="well">
                            <em>You currently have no Contacts. <a href="#add-popup" class="open-popup-link">Add one now.</a></em>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('header_content')
    @parent

    <style>
        .white-popup {
            position: relative;
            background: #FFF;
            padding: 20px;
            width: auto;
            max-width: 500px;
            margin: 20px auto;
        }

        .contact-add-form .row {
            margin-bottom: 5px;
        }
    </style>

@endsection

@section('footer_content')
    @parent

    <script>
        $(".open-popup-link").magnificPopup({
            type: 'inline',
            midClick: true
        });

        $("#search-box").toggle();
        $("#search-results-container").toggle();

        $("#search-go").click(function(){
            searchContacts();
        });

        $("#search_text").keyup(function(ev){
            if(ev.keyCode == 13){
                searchContacts();
            }
        });

        function searchContacts(){
            var srch = $("#search_text").val();
            if(srch.length >= 3){
                srch = srch.replace(/["']/g, "");
                $.get("{{ url('/contact-search') }}/" + encodeURIComponent(srch), {}, function(result){
                    $("#search-results-container").show();
                    $("#search-results").html(result);
                });
            }
        }
    </script>

@endsection