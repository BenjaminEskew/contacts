# Setup and Installation

## Required:
    * git
    * composer
    * MySQL
    * PHP 5.5.9+ (cli)
    * Apache

## Installation:
    1. Create a directory for the project within your web root.
    2. cd to the directory, and execute the following commands in the console:
        * git init
        * git clone git@bitbucket.org:BenjaminEskew/contacts.git
        * cd contacts
        * composer install
    3. Create a local database (MySQL) named "contacts", or whichever name you prefer.
    4. Open the file "<root>/.env.example" and modify the database connection information (DB_HOST, DB_DATABASE, DB_USERNAME, DB_PASSWORD) to your local information, then save the file as "<root>/.env"
    5. Execute the following commands in the console:
        * php artisan key:generate
        * php artisan migrate:install
        * php artisan migrate

## Use:
    1. View the Homepage for the application, at "<root>/public/"
    2. Create a user account by filling out the form at "<root>/public/register" and submitting.
    3. Have fun.