<?php

namespace App\Http\Controllers;

use Auth;
use Input;
use Redirect;

use App\AppHelpers;
use App\Contact;
use App\User;

use App\Http\Requests;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function contactAdd(Request $request)
    {
        $fname = $request->first_name;
        $lname = $request->last_name;
        $em = $request->email;

        if($fname != '' && $lname != '')
        {
            if($em != '')
            {
                if(AppHelpers::validate_email($em))
                {
                    //
                    $chk = Contact::where('email', $em)->where('owner_id', '=', Auth::user()->id)->count();
                    if($chk == 0)
                    {
                        $C = new Contact;
                        $C->owner_id = Auth::user()->id;
                        $C->first_name = $fname;
                        $C->last_name = $lname;
                        $C->email = $em;
                        $C->phone = $request->phone;
                        $C->birthday = $request->birthday;
                        $C->address = $request->address;
                        $C->city_name = $request->city_name;
                        $C->state_name = $request->state_name;
                        $C->country_name = $request->country_name;
                        $C->zip = $request->zip;
                        $C->twitter_id = $request->twitter_id;

                        if($C->address != '' && $C->city_name != '' && $C->state_name != '')
                        {
                            $latlng = AppHelpers::geocode_address($C);

                            if($latlng)
                            {
                                $ret = json_decode($latlng);
                                $C->lat = $ret->lat;
                                $C->lon = $ret->lon;
                            }
                        }

                        $C->save();

                        return \Redirect::to('/contacts')->with('success_message', 'Successfully added new Contact.');
                    }else{
                        return \Redirect::to('/contacts')->with('error_message', 'Contact already exists in your list.');
                    }
                }else{
                    return \Redirect::back()->with('error_message', 'Email address must be valid.');
                }
            }else{
                return \Redirect::back()->with('error_message', 'Email address is required.');
            }
        }else{
            return \Redirect::back()->with('error_message', 'First and Last Name is required.');
        }
    }

    public function contacts(Request $request)
    {
        $r_sort = ($request->sort == '') ? 'ln-asc' : $request->sort;
        $sort = 'ln-asc';
        $r_limit = ($request->limit == '') ? 10 : $request->limit;
        switch($r_limit)
        {
            case 10:
                $limit = 10;
                break;
            case 20:
                $limit = 20;
                break;
            case 50:
                $limit = 50;
                break;
            default:
                $limit = 10;
                break;
        }

        $sort_by = 'last_name';
        $sort_order = 'asc';
        switch($r_sort)
        {
            case 'id-asc':
                $sort_by = 'id';
                $sort_order = 'asc';
                $sort = 'id-asc';
                break;
            case 'id-desc':
                $sort_by = 'id';
                $sort_order = 'desc';
                $sort = 'id-desc';
                break;
            case 'ph-asc':
                $sort_by = 'phone';
                $sort_order = 'asc';
                $sort = 'ph-asc';
                break;
            case 'ph-desc':
                $sort_by = 'phone';
                $sort_order = 'desc';
                $sort = 'ph-desc';
                break;
            case 'ln-asc':
                $sort_by = 'last_name';
                $sort_order = 'asc';
                $sort = 'ln-asc';
                break;
            case 'ln-desc':
                $sort_by = 'last_name';
                $sort_order = 'desc';
                $sort = 'ln-desc';
                break;
            case 'fn-asc':
                $sort_by = 'first_name';
                $sort_order = 'asc';
                $sort = 'fn-asc';
                break;
            case 'fn-desc':
                $sort_by = 'first_name';
                $sort_order = 'desc';
                $sort = 'fn-desc';
                break;
            case 'em-asc':
                $sort_by = 'email';
                $sort_order = 'asc';
                $sort = 'em-asc';
                break;
            case 'em-desc':
                $sort_by = 'email';
                $sort_order = 'desc';
                $sort = 'em-desc';
                break;
            default:
                $sort_by = 'last_name';
                $sort_order = 'asc';
                $sort = 'ln-asc';
                break;
        }

        $contacts = Contact::where('owner_id', Auth::user()->id)
            ->orderBy($sort_by, $sort_order)
            ->paginate($limit);
        $contacts->appends(['sort' => $sort, 'limit' => $limit]);

        return view('contacts', ['contacts' => $contacts, 'sort' => $sort, 'limit' => $limit]);
    }

    public function contactDelete($id)
    {
        $C = Contact::find($id);

        if(is_object($C))
        {
            if($C->owner_id == Auth::user()->id)
            {
                $us = null;
                if($C->user_id != 0)
                {
                    $us = User::find($C->user_id);
                }

                return view('contact_delete', ['item' => $C, 'user_related' => $us]);
            }else{
                return \Redirect::to('/contacts')->with('error_message', 'This Contact does not belong to you.');
            }
        }else{
            return \Redirect::to('/contacts')->with('error_message', 'Contact does not exist.');
        }
    }

    public function contactDeletePost($id)
    {
        $C = Contact::find($id);

        if(is_object($C))
        {
            if($C->owner_id == Auth::user()->id)
            {
                $C->delete();

                return \Redirect::to('/contacts')->with('success_message', 'Successfully deleted Contact.');
            }else{
                return \Redirect::to('/contacts')->with('error_message', 'This Contact does not belong to you.');
            }
        }else{
            return \Redirect::to('/contacts')->with('error_message', 'Contact does not exist.');
        }
    }

    public function contactEdit($id)
    {
        $C = Contact::find($id);

        if(is_object($C))
        {
            if($C->owner_id == Auth::user()->id)
            {
                $us = null;
                if($C->user_id != 0)
                {
                    $us = User::find($C->user_id);
                }

                return view('contact_edit', ['item' => $C, 'user_related' => $us]);
            }else{
                return \Redirect::to('/contacts')->with('error_message', 'This Contact does not belong to you.');
            }
        }else{
            return \Redirect::to('/contacts')->with('error_message', 'Contact does not exist.');
        }
    }

    public function contactEditPost(Request $request, $id)
    {
        $C = Contact::find($id);

        if(is_object($C))
        {
            if($C->owner_id == Auth::user()->id)
            {
                $fname = $request->first_name;
                $lname = $request->last_name;
                $em = $request->email;

                if($fname != '' && $lname != '')
                {
                    if($em != '')
                    {
                        if(AppHelpers::validate_email($em))
                        {
                            $chk = Contact::where('email', $em)->where('owner_id', Auth::user()->id)->where('id', '!=', $C->id)->count();
                            if($chk == 0)
                            {
                                $C->first_name = $fname;
                                $C->last_name = $lname;
                                $C->email = $em;
                                $C->phone = $request->phone;
                                $C->birthday = $request->birthday;
                                $C->address = $request->address;
                                $C->city_name = $request->city_name;
                                $C->state_name = $request->state_name;
                                $C->country_name = $request->country_name;
                                $C->zip = $request->zip;
                                $C->twitter_id = $request->twitter_id;

                                if($C->address != '' && $C->city_name != '' && $C->state_name != '')
                                {
                                    $latlng = AppHelpers::geocode_address($C);

                                    if($latlng)
                                    {
                                        $ret = json_decode($latlng);
                                        $C->lat = $ret->lat;
                                        $C->lon = $ret->lon;
                                    }
                                }else{
                                    $C->lat = '';
                                    $C->lon = '';
                                }

                                $C->save();

                                return \Redirect::to('/contacts')->with('success_message', 'Successfully updated Contact.');
                            }else{
                                return \Redirect::back()->with('error_message', 'Contact already exists in your list.');
                            }
                        }else{
                            return \Redirect::back()->with('error_message', 'Email address must be valid.');
                        }
                    }else{
                        return \Redirect::back()->with('error_message', 'Email address is required.');
                    }
                }else{
                    return \Redirect::back()->with('error_message', 'First and Last Name is required.');
                }
            }else{
                return \Redirect::to('/contacts')->with('error_message', 'This Contact does not belong to you.');
            }
        }else{
            return \Redirect::to('/contacts')->with('error_message', 'Contact does not exist.');
        }
    }

    public function contactView($id)
    {
        $C = Contact::find($id);

        if(is_object($C))
        {
            if($C->owner_id == Auth::user()->id)
            {
                $us = null;
                if($C->user_id != 0)
                {
                    $us = User::find($C->user_id);
                }

                return view('contact', ['item' => $C, 'user_related' => $us]);
            }else{
                return \Redirect::to('/contacts')->with('error_message', 'This Contact does not belong to you.');
            }
        }else{
            return \Redirect::to('/contacts')->with('error_message', 'Contact does not exist.');
        }
    }

    public function editProfile()
    {
        return view('edit_profile');
    }

    public function editProfilePost(Request $request)
    {
        $U = User::find(Auth::user()->id);

        $fname = $request->first_name;
        $lname = $request->last_name;
        $em = $request->email;

        if($fname != '' && $lname != '')
        {
            if($em != '')
            {
                if(AppHelpers::validate_email($em))
                {
                    $chk = User::where('email', $em)->where('id', '!=', Auth::user()->id)->count();
                    if($chk == 0)
                    {
                        $U->name = $request->name;
                        $U->first_name = $fname;
                        $U->last_name = $lname;
                        $U->email = $em;
                        $U->phone = $request->phone;
                        $U->birthday = $request->birthday;
                        $U->address = $request->address;
                        $U->city_name = $request->city_name;
                        $U->state_name = $request->state_name;
                        $U->country_name = $request->country_name;
                        $U->zip = $request->zip;
                        $U->twitter_id = $request->twitter_id;

                        if($U->address != '' && $U->city_name != '' && $U->state_name != '')
                        {
                            $latlng = AppHelpers::geocode_address($U);

                            if($latlng)
                            {
                                $ret = json_decode($latlng);
                                $U->lat = $ret->lat;
                                $U->lon = $ret->lon;
                            }
                        }

                        $U->save();

                        return \Redirect::to('/edit-account')->with('success_message', 'Successfully updated Account Details.');
                    }else{
                        return \Redirect::back()->with('error_message', 'Email address is already associated with another account.');
                    }
                }else{
                    return \Redirect::back()->with('error_message', 'Email address must be valid.');
                }
            }else{
                return \Redirect::back()->with('error_message', 'Email address is required.');
            }
        }else{
            return \Redirect::back()->with('error_message', 'First and Last Name is required.');
        }
    }
}
