<?php

namespace App\Http\Controllers;

use Redirect;

//use App\AppHelpers;
use App\User;

use Illuminate\Http\Request;

use App\Http\Requests;

class PageController extends Controller
{
    public function index()
    {
        return view('welcome');
    }

    public function profile($id)
    {
        $U = User::find($id);

        if(is_object($U))
        {
            return view('user_profile', ['item' => $U]);
        }else{
            return \Redirect::to('/')->with('error_message', 'User does not exist.');
        }
    }
}
