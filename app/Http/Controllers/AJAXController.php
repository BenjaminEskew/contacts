<?php

namespace App\Http\Controllers;

use Auth;
use DB;

use App\Contact;
use App\User;

use Illuminate\Http\Request;

use App\Http\Requests;

class AJAXController extends Controller
{
    //
    protected $terms;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->terms = '';
    }

    public function contactSearch($terms)
    {
        $this->prepareTerms($terms);

        if($this->terms != '' && strlen($this->terms) >= 3)
        {
            $this->terms = '%' . strtolower($this->terms) . '%';

            $q = DB::select(
                DB::raw("SELECT * FROM contacts WHERE owner_id = ".Auth::user()->id." and (lower(`first_name`) like :the_terms or lower(`last_name`) like :the_terms2 or `phone` like :the_terms3 or lower(`address`) like :the_terms4 or lower(`city_name`) like :the_terms5 or lower(`state_name`) like :the_terms6 or lower(`country_name`) like :the_terms7 or lower(`zip`) like :the_terms8) order by `last_name` asc"),
                array('the_terms' => $this->terms, 'the_terms2' => $this->terms, 'the_terms3' => $this->terms, 'the_terms4' => $this->terms, 'the_terms5' => $this->terms, 'the_terms6' => $this->terms, 'the_terms7' => $this->terms, 'the_terms8' => $this->terms)
            );

            if(count($q) >= 1)
            {
                $ret = '<h3>Search Results: ' . number_format(count($q)) . ' Found</h3>
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Location</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        ';
                        foreach($q as $qu)
                        {
                            $ret .= '<tr>
                                <td>' . $qu->id . '</td>
                                <td>' . $qu->first_name . '</td>
                                <td>' . $qu->last_name . '</td>
                                <td>' . $qu->email . '</td>
                                <td>' . $qu->phone . '</td>
                                <td>' . ($qu->city_name != '' ? $qu->city_name . ', ' : '') . ($qu->state_name != '' ? $qu->state_name : '') . '</td>
                                <td><a href="' . url('/contact-edit') . '/' . $qu->id . '">Edit</a> / <a href="' . url('/contact-delete') . '/' . $qu->id . '">Delete</a></td>
                            </tr>';
                        }
                $ret .= '
                    </tbody>
                </table>';

                return $ret;
            }else{
                return '<em>No results found.</em>';
            }
        }else{
            return '<em>Search terms required.</em>';
        }
    }

    private function prepareTerms($terms)
    {
        $this->terms = strip_tags(urldecode($terms));
        $this->terms = str_replace('"', '', str_replace("'", '', $this->terms));
    }
}
