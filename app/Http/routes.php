<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/', 'PageController@index');

    Route::get('contacts', 'HomeController@contacts');
    Route::get('dashboard', 'HomeController@index');

    Route::get('edit-account', 'HomeController@editProfile');
    Route::post('edit-account', 'HomeController@editProfilePost');

    Route::get('profile/{id}', 'PageController@profile');
    Route::get('contact/{id}', 'HomeController@contactView');

    Route::post('contact-add', 'HomeController@contactAdd');

    Route::get('contact-edit/{id}', 'HomeController@contactEdit');
    Route::post('contact-edit/{id}', 'HomeController@contactEditPost');
    Route::get('contact-delete/{id}', 'HomeController@contactDelete');
    Route::post('contact-delete/{id}', 'HomeController@contactDeletePost');

    Route::get('contact-search/{terms}', 'AJAXController@contactSearch');
});
