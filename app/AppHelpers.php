<?php

namespace App;

class AppHelpers {
    public static function geocode_address($userOrContact)
    {
        $q = [];
        $q[] = $userOrContact->address ?: '';
        $q[] = $userOrContact->city_name ?: '';
        $q[] = $userOrContact->state_name ?: '';
        $q[] = $userOrContact->zip ?: '';
        $q[] = $userOrContact->country_name ?: '';

        $qu = trim(implode(',', array_filter($q)));
        $qu = str_replace(' ', '+', $qu);

        $r = file_get_contents('https://maps.google.com/maps/api/geocode/json?address=' . $qu);

        if(empty($r))
        {
            return false;
        }

        $outp = json_decode($r);

        if(count($outp->results) == 0 || empty($outp->results[0]))
        {
            return false;
        }

        $res = $outp->results[0];
        if($goog = $res->geometry)
        {
            return json_encode(array('lat' => $goog->location->lat, 'lon' => $goog->location->lng));
        }

        return false;
    }

    public static function validate_email($email)
    {
        return (filter_var($email, FILTER_VALIDATE_EMAIL));
    }
}